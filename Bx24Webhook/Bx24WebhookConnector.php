<?php

namespace Databridge\Connectors\Bx24Webhook;
include_once 'Translators/ContactTranslator.php';
include_once 'Translators/DealTranslator.php';
include_once 'Translators/ProcessTranslator.php';
include_once 'Translators/CompanyTranslator.php';
include_once 'Translators/RequisitesTranslator.php';
include_once 'Logs/Log.php';

use Databridge\Connectors\Bx24Webhook\Logs\Log;
use Databridge\Connectors\Bx24Webhook\Translators\ContactTranslator;
use Databridge\Connectors\Bx24Webhook\Translators\DealTranslator;
use Databridge\Connectors\Bx24Webhook\Translators\ProcessTranslator;
use Databridge\Connectors\Bx24Webhook\Translators\CompanyTranslator;
use Databridge\Connectors\Bx24Webhook\Translators\RequisitesTranslator;

class Bx24WebhookConnector
{
    private $domainUrl;
    private $timeout;
    private $loopCount;
    private $pause;
    private $maxCount;
    private $currentCount = 0;
    private $log;
    private $contactTranslator;
    private $processTranslator;
    private $dealTranslator;
    private $companyTranslator;
    private $requisitesTranslator;

    function __construct( $url, $timeout = 15, $loopCount = 3, $pause = 16, $maxCount = 255 )
    {
        $this->timeout = $timeout;
        $this->loopCount = $loopCount;
        $this->domainUrl = $url;
        $this->pause = $pause;
        $this->maxCount = $maxCount;
        $this->contactTranslator = new ContactTranslator();
        $this->dealTranslator = new DealTranslator();
        $this->processTranslator = new ProcessTranslator();
        $this->companyTranslator = new CompanyTranslator();
        $this->requisitesTranslator = new RequisitesTranslator();
        $log = new Logs\Log();
        $this->log = $log;
        $log->log_info( "timeout: $this->timeout; loopCount: $this->loopCount; domainUrl: $this->domainUrl; pause: $this->pause; 
        maxCount: $this->maxCount." );

    }

    public function addContact( $contact )
    {
        $fields = $this->contactTranslator->translateToBitrixCreate( $contact );
        $queryUrl = $this->domainUrl . 'crm.contact.add.json';
        $params = [
            'fields' => $fields,
            'params' => [ 'REGISTER_SONET_EVENT' => 'N' ]
        ];
        $queryData = http_build_query( $params );
        $this->log->log_debug( $params );
        $response = $this->getData( $queryData, $queryUrl );
        if ( ! isset( $response['error'] ) ) {
            return $response['result'];
        } else {
            return $response;
        }
    }

    public function getContacts( $filters, $dataForSelect, $maxElements )
    {
        $page = 0;
        $data = [];
        $total = $maxElements;
        $queryUrl = $this->domainUrl . 'crm.contact.list.json';
        $start = $page * 50;
        $isError = false;
        while ( ( $total >= $start ) && ( $start <= $maxElements ) && ! $isError ) {
            $response = $this->requestToGetData( $filters, $dataForSelect, $start, $queryUrl );
            if ( isset( $response['error'] ) ) {
                $isError = true;
                $data['error'] = $response;
            } else {
                $total = $response['total'];
                $data = array_merge( $data, $response['result'] );
                $page++;
                $start = $page * 50;
            }
        }

        return $data;
    }

    public function updateContact( $id, $contact )
    {
        $fields = $this->contactTranslator->translateToBitrixUpdate( $contact );
        $queryUrl = $this->domainUrl . 'crm.contact.update.json';
        $params = [
            'id' => $id,
            'fields' => $fields,
            'params' => [ 'REGISTER_SONET_EVENT' => 'N' ]
        ];
        $queryData = http_build_query( $params );
        $this->log->log_debug( $params );
        $response = $this->getData( $queryData, $queryUrl );

        if ( ! isset( $response['error'] ) ) {
            return $response['result'];
        } else {
            return $response;
        }
    }

    public function requestToGetData( $filters, $dataForSelect, $start, $queryUrl )
    {
        $params = [
            'order' => [ 'ID' => 'ASC' ],
            'filter' => $filters,
            'select' => $dataForSelect,
            'start' => $start
        ];
        $queryData = http_build_query( $params );
        $this->log->log_debug( $params );

        return $this->getData( $queryData, $queryUrl );
    }

    public function addDeal( $deal )
    {
        $fields = $this->dealTranslator->translateToBitrixCreate( $deal );
        $queryUrl = $this->domainUrl . 'crm.deal.add.json';
        $params = [
            'fields' => $fields,
            'params' => [ 'REGISTER_SONET_EVENT' => 'N' ]
        ];
        $queryData = http_build_query( $params );
        $this->log->log_debug( $params );
        $response = $this->getData( $queryData, $queryUrl );
        if ( ! isset( $response['error'] ) ) {
            return $response['result'];
        } else {
            return $response;
        }
    }

    public function getDeals( $filters, $dataForSelect, $maxElements )
    {
        $page = 0;
        $data = [];
        $total = $maxElements;
        $queryUrl = $this->domainUrl . 'crm.deal.list.json';
        $start = $page * 50;
        $isError = false;
        while ( ( $total >= $start ) && ( $start <= $maxElements ) && ! $isError ) {
            $response = $this->requestToGetData( $filters, $dataForSelect, $start, $queryUrl );

            if ( isset( $response['error'] ) ) {
                $isError = true;
                $data['error'] = $response;
            } else {
                $total = $response['total'];
                $data = array_merge( $data, $response['result'] );
                $page++;
                $start = $page * 50;
            }
        }

        return $data;
    }

    public function updateDeal( $id, $deal )
    {
        $fields = $this->dealTranslator->translateToBitrixUpdate( $deal );
        $queryUrl = $this->domainUrl . 'crm.deal.update.json';
        $params = [
            'id' => $id,
            'fields' => $fields,
            'params' => [ 'REGISTER_SONET_EVENT' => 'N' ]
        ];
        $queryData = http_build_query( $params );
        $this->log->log_debug( $params );
        $response = $this->getData( $queryData, $queryUrl );

        if ( ! isset( $response['error'] ) ) {
            return $response['result'];
        } else {
            return $response;
        }
    }

    public function addProcess( $process )
    {
        $fields = $this->processTranslator->translateToBitrixCreate( $process );
        $queryUrl = $this->domainUrl . 'lists.element.add.json';
        $params = [
            $fields,
            'params' => [ 'REGISTER_SONET_EVENT' => 'N' ]
        ];
        $queryData = http_build_query( $params );
        $this->log->log_debug( $params );

        return $this->getData( $queryData, $queryUrl );
    }

    public function getProcess( $filters, $maxElements )
    {
        $page = 0;
        $data = [];
        $total = $maxElements;
        $queryUrl = $this->domainUrl . 'lists.element.get.json';
        $start = $page * 50;
        $isError = false;
        while ( ( $total >= $start ) && ( $start <= $maxElements ) && ! $isError ) {
            $response = $this->requestToGetDataForProcess( $filters, $start, $queryUrl );
            if ( isset( $response['error'] ) ) {
                $isError = true;
                $data['error'] = $response;
            } else {
                $total = $response['total'];
                $data = array_merge( $data, $response['result'] );
                $page++;
                $start = $page * 50;
            }
        }

        return $data;
    }

    public function updateProcess( $id, $process )
    {
        $fields = $this->processTranslator->translateToBitrixUpdate( $process );
        $queryUrl = $this->domainUrl . 'lists.element.update.json';
        $params = [
            $fields,
            'ELEMENT_ID' => $id,//or your can use ELEMENT_CODE
            'params' => [ 'REGISTER_SONET_EVENT' => 'N' ]
        ];
        $queryData = http_build_query( $params );
        $this->log->log_debug( $params );
        $response = $this->getData( $queryData, $queryUrl );

        if ( ! isset( $response['error'] ) ) {
            return $response['result'];
        } else {
            return $response;
        }
    }

    public function requestToGetDataForProcess( $filters, $start, $queryUrl )
    {
        $params = [
            'IBLOCK_TYPE_ID' => 'bitrix_processes',
            'IBLOCK_ID' => $filters['PROCESS_ID'],
            'ELEMENT_ORDER' => [ 'ID' => 'ASC' ],
            'filter' => $filters['filters'],
            'start' => $start
        ];
        $queryData = http_build_query( $params );
        $this->log->log_debug( $params );

        return $this->getData( $queryData, $queryUrl );
    }

    public function addCompanyWithRequisites( $company )
    {
        $fields = $this->companyTranslator->translateToBitrixCreate( $company );
        $queryUrl = $this->domainUrl . 'crm.company.add.json';
        $params = array_merge( $fields, [
            'params' => [ 'REGISTER_SONET_EVENT' => 'N' ]
        ] );
        $queryData = http_build_query( $params );
        $this->log->log_debug( $params );

        $response = $this->getData( $queryData, $queryUrl );
        if ( ! empty( $response['result'] ) ) {
            $companyId = $response['result'];
            $company['ENTITY_ID'] = $companyId;
            $fields = $this->requisitesTranslator->translateToBitrixCreate( $company );

            $queryUrl = $this->domainUrl . 'crm.requisite.add.json';
            $params = array_merge( $fields, [
                'params' => [ 'REGISTER_SONET_EVENT' => 'N' ]
            ] );
            $queryData = http_build_query( $params );
            $this->log->log_debug( $params );
            $requisitesResponse = $this->getData( $queryData, $queryUrl );
            return [
                'companyId' => $companyId,
                'requisitesId' => $requisitesResponse['result']
            ];
        } else {
            return $response;
        }
    }

    public function updateCompanyByRequisitesINN( $companyData )
    {
        $filters = $this->requisitesTranslator->translateToBitrixUpdate( $companyData );
        $response = $this->getCompanyByRequisitesINN( $filters, 500 );
        if ( empty( $response ) || isset( $response['error'] ) ) {
            return $response;
        }
        $companies = $response;
        foreach ( $companies as $key => $company ) {
            $response = $this->updateCompanyById( $company['ID'], $companyData );
            if ( isset( $response['error'] ) ) {
                return $response;
            }
        }
        return $response;
    }

    public function updateCompanyById( $id, $company )
    {
        $fields = $this->companyTranslator->translateToBitrixUpdate( $company );
        $queryUrl = $this->domainUrl . 'crm.company.update.json';
        $params = [
            'id' => $id,
            'params' => [ 'REGISTER_SONET_EVENT' => 'N' ]
        ];
        $queryData = http_build_query( array_merge( $fields, $params ) );
        $this->log->log_debug( $params );
        $response = $this->getData( $queryData, $queryUrl );

        if ( ! isset( $response['error'] ) ) {
            return $response['result'];
        } else {
            return $response;
        }
    }

    public function getCompanyByRequisitesINN( $requestFilters, $maxElements )
    {
        $page = 0;
        $data = [];
        $total = $maxElements;
        $queryUrl = $this->domainUrl . 'crm.requisite.list.json';
        $start = $page * 50;
        $isError = false;
        $dataForSelect = [ "ENTITY_ID", "RQ_INN" ];
        $filters = $this->requisitesTranslator->translateToBitrixGet( $requestFilters );
        while ( ( $total >= $start ) && ( $start <= $maxElements ) && ! $isError ) {
            $response = $this->requestToGetData( $filters, $dataForSelect, $start, $queryUrl );
            if ( isset( $response['error'] ) ) {
                $isError = true;
                $data['error'] = $response;
            } else {
                $total = $response['total'];
                $data = array_merge( $data, $response['result'] );
                $page++;
                $start = $page * 50;
            }
        }
        $queryUrl = $this->domainUrl . 'crm.company.list.json';
        $dataForSelect = [ "ID", "TITLE" ];
        if ( ! isset( $data['error'] ) && ! empty( $data ) ) {
            foreach ( $data as $key => $requisite ) {
                $filters = $this->companyTranslator->translateToBitrixGet( $requestFilters, $requisite['ENTITY_ID'] );
                $response = $this->requestToGetCompanyData( $filters, $dataForSelect, $queryUrl );
                if ( ! isset( $response['error'] ) && ! empty( $response['result'] ) ) {
                    $company = array_pop( $response['result'] );
                    if ( ! empty( $company ) ) {
                        $data[$key]['company'] = array_merge( $company, [ 'RQ_INN' => $requisite['RQ_INN'] ] );
                    }
                }
            }
        }
        if ( ! empty( $data ) && ! isset( $data['error'] ) ) {
            $data = array_column( $data, 'company' );
        }
        return $data;
    }

    private function requestToGetCompanyData( $filters, $dataForSelect, $queryUrl )
    {
        $params = [
            'order' => [ 'ID' => 'ASC' ],
            'filter' => $filters,
            'select' => $dataForSelect
        ];
        $queryData = http_build_query( $params );
        $this->log->log_debug( $params );

        return $this->getData( $queryData, $queryUrl );
    }

    private function count()
    {
        $this->currentCount++;
        if ( $this->currentCount >= $this->maxCount ) {
            $this->log->log_warning( "$this->pause sec timeout!" );
            $this->currentCount = 0;
            sleep( $this->pause );
        }
    }

    private function getData( $queryData, $queryUrl )
    {
        $isError = true;
        $i = 1;
        $result = [];
        while ( $isError && ( $i <= $this->loopCount ) ) {
            $this->count();
            $isError = false;
            $errorMessage = '';
            $curl = curl_init();
            curl_setopt_array( $curl, [
                CURLOPT_SSL_VERIFYPEER => 0,
                CURLOPT_POST => 1,
                CURLOPT_HEADER => 0,
                CURLOPT_RETURNTRANSFER => 1,
                CURLOPT_URL => $queryUrl,
                CURLOPT_POSTFIELDS => $queryData,
            ] );

            $result = curl_exec( $curl );
            $result = json_decode( $result, true );

            $this->log->log_debug( "URL: $queryUrl" );
            $this->log->log_debug( $result );
            $responseCode = curl_getinfo( $curl, CURLINFO_HTTP_CODE );
            if ( $responseCode >= 400 ) {
                $isError = true;
                $errorMessage = 'HTTP Error: ' . $responseCode;
                $this->log->log_warning( $errorMessage );
            } else if ( $result === false || ! isset( $result['result'] ) ) {
                $isError = true;
                $errorMessage = 'CURL Error: ' . curl_error( $curl ) . 'something wrong with response!';
                $this->log->log_warning( $errorMessage );
            }
            $i++;
            if ( ! $isError ) {
                curl_close( $curl );
            } elseif ( $i <= $this->loopCount ) {
                $this->log->log_warning( "$this->timeout sec timeout!" );
                sleep( $this->timeout );
            } elseif ( $i > $this->loopCount ) {
                $result = [
                    'error' => $isError,
                    'error_description' => $errorMessage
                ];
            }
        }
        return $result;
    }
}