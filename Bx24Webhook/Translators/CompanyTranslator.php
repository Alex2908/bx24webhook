<?php

namespace Databridge\Connectors\Bx24Webhook\Translators;
include_once 'BitrixTranslator.php';

class CompanyTranslator implements BitrixTranslator
{

    public function translateToBitrixCreate( $company )
    {
        $fields = [
            'fields' => [
                'TITLE' => $company['NAME']
            ]
        ];

        return $fields;

    }

    public function translateToBitrixUpdate( $company )
    {
        $fields = [
            'fields' => [
                'TITLE' => $company['NAME']
            ]
        ];

        return $fields;
    }

    public function translateToBitrixGet( $company, $id = "" )
    {
        $filters = [
            'TITLE' => $company['NAME'] ?: '',
            'PRESET_ID' => $company['PRESET_ID'] ?: '',
            'ID' => $id
        ];

        return array_filter( $filters );
    }
}