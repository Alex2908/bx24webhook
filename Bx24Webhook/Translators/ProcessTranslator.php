<?php

namespace Databridge\Connectors\Bx24Webhook\Translators;
include_once 'BitrixTranslator.php';

class ProcessTranslator implements BitrixTranslator
{

    public function translateToBitrixCreate( $process )
    {
        $fields = [
            'IBLOCK_TYPE_ID' => 'bitrix_processes',
            'IBLOCK_ID' => $process['PROCESS_ID'],
            'ELEMENT_CODE' => 'element_' . time(),
            'fields' => [
                'NAME' => $process['NAME'],
                'PROPERTY_103' => $process['BLAGOPOLUCHATEL']
            ]
        ];

        return $fields;
    }

    public function translateToBitrixUpdate( $process )
    {
        $fields = [
            'NAME' => $process['NAME'] ?: '',
            'PROPERTY_103' => $process['BLAGOPOLUCHATEL'] ?: ''
        ];
        $fields = array_filter( $fields );
        $fields = [
            'IBLOCK_TYPE_ID' => 'bitrix_processes',
            'IBLOCK_ID' => $process['PROCESS_ID'],
            'fields' => $fields,
        ];

        return $fields;
    }
}