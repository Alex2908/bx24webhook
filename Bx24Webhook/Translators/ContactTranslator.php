<?php

namespace Databridge\Connectors\Bx24Webhook\Translators;
include_once 'BitrixTranslator.php';

class ContactTranslator implements BitrixTranslator
{

    public function translateToBitrixCreate( $contact )
    {
        $fields = [
            'NAME' => $contact['NAME'],
            'LAST_NAME' => $contact['LAST_NAME'],
            'SOURCE_ID' => $contact['SOURCE_ID'],
            'TYPE_ID' => $contact['TYPE_ID'],
            'PHONE' => [ [ 'VALUE' => $contact['PHONE'], 'VALUE_TYPE' => 'WORK' ] ],
            'WORK_PHONE' => $contact['PHONE'],
            'EMAIL' => [ [ 'VALUE' => $contact['EMAIL'], 'VALUE_TYPE' => 'WORK' ] ],
            'PERSONAL_GENDER' => $contact['GENDER'],
            'UF_CRM_1574414108' => $contact['LAST_DEAL_DATE'],
            'UF_CRM_1574413185' => $contact['PROGRAMS']
        ];

        return $fields;
    }

    public function translateToBitrixUpdate( $contact )
    {
        $fields = [
            'NAME' => $contact['NAME'] ?: '',
            'LAST_NAME' => $contact['LAST_NAME'] ?: '',
            'SOURCE_ID' => $contact['SOURCE_ID'] ?: '',
            'TYPE_ID' => $contact['TYPE_ID'] ?: '',
            'PHONE' => $contact['PHONE'] ? [ [ 'VALUE' => $contact['PHONE'], 'VALUE_TYPE' => 'WORK' ] ] : '',
            'WORK_PHONE' => $contact['PHONE'] ?: '',
            //need to set EMAIL ID from get request or it will add new email if it not exists
            'EMAIL' => $contact['EMAIL'] ? [ [ 'VALUE' => $contact['EMAIL'], 'VALUE_TYPE' => 'WORK', 'ID' => $contact['EMAIL_ID'] ] ] : '',
            'PERSONAL_GENDER' => $contact['GENDER'] ?: '',
            'UF_CRM_1574414108' => $contact['LAST_DEAL_DATE'] ?: '',
            'UF_CRM_1574413185' => $contact['PROGRAMS'] ?: ''
        ];
        $fields = array_filter( $fields );

        return $fields;
    }
}