<?php

namespace Databridge\Connectors\Bx24Webhook\Translators;
include_once 'BitrixTranslator.php';

class DealTranslator implements BitrixTranslator
{

    public function translateToBitrixCreate( $deal )
    {
        $fields = [
            'TITLE' => $deal['TITLE'],
            'TYPE_ID' => $deal['TYPE_ID'],
            'STAGE_ID' => $deal['STAGE_ID'],
            'CONTACT_ID' => $deal['CONTACT_ID'],
            'CURRENCY_ID' => $deal['CURRENCY_ID'],
            'OPPORTUNITY' => $deal['OPPORTUNITY'],
            'BEGINDATE' => $deal['BEGINDATE'],
            'SOURCE_ID' => $deal['SOURCE_ID'],
            'UF_CRM_1575370850' => $deal['BLAGOPOLUCHATEL'],
            'UF_CRM_1574412556' => $deal['PROGRAM']
        ];

        return $fields;
    }

    public function translateToBitrixUpdate( $deal )
    {
        //stages works with C param (ex: 'C1:WON')
        $fields = [
            'TITLE' => $deal['TITLE'] ?: '',
            'TYPE_ID' => $deal['TYPE_ID'] ?: '',
            'STAGE_ID' => $deal['STAGE_ID'] ?: '',
            'CONTACT_ID' => $deal['CONTACT_ID'] ?: '',
            'CURRENCY_ID' => $deal['CURRENCY_ID'] ?: '',
            'OPPORTUNITY' => $deal['OPPORTUNITY'] ?: '',
            'BEGINDATE' => $deal['BEGINDATE'] ?: '',
            'SOURCE_ID' => $deal['SOURCE_ID'] ?: '',
            'UF_CRM_1575370850' => $deal['BLAGOPOLUCHATEL'] ?: '',
            'UF_CRM_1574412556' => $deal['PROGRAM'] ?: ''
        ];
        $fields = array_filter( $fields );

        return $fields;
    }
}