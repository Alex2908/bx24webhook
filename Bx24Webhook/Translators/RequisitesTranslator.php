<?php

namespace Databridge\Connectors\Bx24Webhook\Translators;
include_once 'BitrixTranslator.php';

class RequisitesTranslator implements BitrixTranslator
{

    public function translateToBitrixCreate( $requisites )
    {
        $fields = [
            'fields' => [
                'ENTITY_TYPE_ID' => 4,
                'ENTITY_ID' => $requisites['ENTITY_ID'],
                'PRESET_ID' => $requisites['PRESET_ID'],
                'NAME' => $requisites['NAME'],
                'RQ_INN' => $requisites['RQ_INN']
            ]
        ];

        return $fields;
    }

    public function translateToBitrixUpdate( $requisites )
    {
        $fields = [
            'RQ_INN' => $requisites['RQ_INN']
        ];

        return $fields;
    }

    public function translateToBitrixGet( $requisites )
    {
        $filters = [
            'RQ_INN' => $requisites['RQ_INN'] ?: '',
            'PRESET_ID' => $requisites['PRESET_ID'] ?: ''
        ];

        return array_filter( $filters );
    }
}