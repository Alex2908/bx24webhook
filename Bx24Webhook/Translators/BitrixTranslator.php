<?php

namespace Databridge\Connectors\Bx24Webhook\Translators;

interface BitrixTranslator
{
    public function translateToBitrixCreate( $data );

    public function translateToBitrixUpdate( $data );
}